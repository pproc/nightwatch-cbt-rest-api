# Nightwatch.js - usage of crossBrowserTesting.com REST api

## Introduction

Repository allows [Nightwatch.js](http://nightwatchjs.org/) library to send REST requests to [crossBrowserTesting](https://crossbrowsertesting.com) cloud.

It uses [request library](https://www.npmjs.com/package/request). 

For crossBrowserTesting.com API documentation refer to: [https://crossbrowsertesting.com/apidocs/v3/](https://crossbrowsertesting.com/apidocs/v3/)

Repository was created during my full time job in NetEnt company in Krakow, Poland.

## Install

```sh
  npm install --save-dev nightwatch-cbt-rest-api
```

## Usage

Add the following `beforeEach`/`afterEach` or `before`/`after` hooks:
```js
const requestSender = require("@netent/nightwatch-cbt-rest-api");

module.exports = {
  before: async function () {
    client.globals.seleniumTestId = await requestSender.getSeleniumTestId(client);
  }
}
```

Use any of the functions displayed by this module:
```js
const requestSender = require("@netent/nightwatch-cbt-rest-api");

...
requestSender.setStatus(client, "success");
```

or your crossBrowserTesting.com API yourself ([documentation](https://crossbrowsertesting.com/apidocs/v3/selenium.html#!/default/put_selenium_selenium_test_id))
```js
const requestSender = require("@netent/nightwatch-cbt-rest-api");

...
requestSender.putCbt(client, "/selenium/{selenium_test_id}", { action: "set_score", score: status });
```

## Author
[Piotr Proc](piotr.proc@gmail.com)