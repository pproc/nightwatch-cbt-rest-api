const requestSender = require("../request/cbtRestRequester");
const assignVariable = require("../utils/callbacks").assignVariable;

const getSeleniumTestId = async function(client) {
    const nightwatchSession = {};

    await client.session(assignVariable(nightwatchSession));

    const cbtSessions = await requestSender.getCbt(client, "/selenium");
    const sessionId = nightwatchSession.value["webdriver.remote.sessionid"];
    const seleniumTestsId = cbtSessions.selenium.filter((x) => x.selenium_session_id === sessionId);

    if (seleniumTestsId.length === 0) {
        throw new Error("There is no Selenium Test running on session: " + sessionId);
    }

    return seleniumTestsId[0].selenium_test_id;
};

module.exports = getSeleniumTestId;
