const requestSender = require("./restRequester");
const urlHanlder = require("../utils/url");

const cbtRestRequester = {
    getCbt: async function(client, view, params = { json: true }) {
        const link = await urlHanlder.getCbtApiUrl(client, view, params);

        return requestSender.get(link);
    },

    postCbt: async function(client, view, params = { json: true }) {
        const link = await urlHanlder.getCbtApiUrl(client, view);

        return requestSender.post(link, params);
    },

    putCbt: async function(client, view, params = { json: true }) {
        const link = await urlHanlder.getCbtApiUrl(client, view);

        return requestSender.put(link, { form: params });
    }
};

module.exports = cbtRestRequester;
