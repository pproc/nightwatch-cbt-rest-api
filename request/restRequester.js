const request = require("request");

const restRequester = {
    get: async function(apiUrl, params = { json: true }) {
        return new Promise((resolve, reject) => {
            request.get(apiUrl, params, (error, response, data) => {
                if (error) {
                    reject(error);
                }
                else {
                    resolve(data);
                }
            });
        });
    },

    post: async function(apiUrl, params = { json: true }) {
        return new Promise((resolve, reject) => {
            request.post(apiUrl, params, (error, response, data) => {
                if (error) {
                    reject(error);
                }
                else {
                    resolve(data);
                }
            });
        });
    },

    put: async function(apiUrl, params = { json: true }) {
        return new Promise((resolve, reject) => {
            request.put(apiUrl, params, (error, response, data) => {
                if (error) {
                    reject(error);
                }
                else {
                    resolve(data);
                }
            });
        });
    }
};

module.exports = restRequester;
