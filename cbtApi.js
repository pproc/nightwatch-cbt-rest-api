const requestSender = require("./request/cbtRestRequester");
const getSeleniumTestId = require("./apiExtend/getSeleniumTestId");

const cbtApiExtenstion = {
    getSeleniumTestId: async function(client) {
        return getSeleniumTestId(client);
    },

    setStatus: async function(client, status) {
        return requestSender.putCbt(client, "/selenium/{selenium_test_id}", { action: "set_score", score: status });
    }
};

Object.assign(cbtApiExtenstion, requestSender);

module.exports = cbtApiExtenstion;
