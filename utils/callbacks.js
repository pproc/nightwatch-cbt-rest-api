/* eslint-disable no-param-reassign */

const assignVariable = (variable) => {
    return (result) => {
        variable.value = result.value;
    };
};

module.exports = { assignVariable };
