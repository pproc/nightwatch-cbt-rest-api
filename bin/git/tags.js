const { echo, exec } = require("shelljs");

const addTag = (version) => {
    echo("");

    try {
        exec(`git tag -a -f -m v${version} v${version}`);

        echo(`Created git tag v${version}`);
    }
    catch (error) {
        echo(`Failed to create git tag v${version}`);
        echo(error);
        process.exit(1);
    }

    echo("");

    try {
        exec(`git push origin v${version}`);

        echo(`Tag v${version} pushed to origin`);
    }
    catch (error) {
        echo(`Failed to push git tag to origin v${version}`);
        echo(error);
        process.exit(1);
    }
};

module.exports = { addTag };

