const { echo, exec } = require("shelljs");

const push = (version) => {
    echo("");

    try {
        exec(`git commit -a -m "feat(packages): Update to version ${version}"`);

        echo(`Make git commit v${version}`);
    }
    catch (error) {
        echo("Failed to make git commit");
        echo(error);
        process.exit(1);
    }

    echo("");

    try {
        exec("git push");

        echo("Commit pushed to origin");
    }
    catch (error) {
        echo("Failed to push commit to origin");
        echo(error);
        process.exit(1);
    }
};

module.exports = { push };

