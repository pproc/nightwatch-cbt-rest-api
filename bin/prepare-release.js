// This script increments version in packges.json and push this change to repository

const fs = require("fs");
const chalk = require("chalk");
const { echo } = require("shelljs");
const semver = require("semver");
const { push } = require("./git/commits");

echo("# Versions");

const packageJSON = JSON.parse(fs.readFileSync("package.json", "utf-8"));
const nextVersion = semver.inc(packageJSON.version, "patch");

packageJSON.version = `${nextVersion}`;
fs.writeFileSync("package.json", JSON.stringify(packageJSON, null, 2));
push(packageJSON.version);

echo(`version: ${chalk.green(packageJSON.name)} - ${chalk.green(packageJSON.version)}`);
