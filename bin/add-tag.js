// This script adds tag to every commit on master branch

const fs = require("fs");
const { echo, exec } = require("shelljs");
const { addTag } = require("./git/tags");

echo("# Versions");

const currentBranch = exec("git rev-parse --abbrev-ref HEAD").stdout.replace(/\r?\n|\r/g, "");
const isRelease = currentBranch === "master";

const packageJSON = JSON.parse(fs.readFileSync("package.json", "utf-8"));

if (isRelease) {
    addTag(packageJSON.version);
}
